package app.mtproject.test;

import org.junit.Assert;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.TextView;
import app.mtproject.*;
import app.mtproject.R;

import com.jayway.android.robotium.solo.Solo;

public class SplashScreenClassTest extends ActivityInstrumentationTestCase2<SplashScreen> {
	Solo solo;
	SplashScreen splashScreenActivity;
	TextView instructions;
	Button acceptBtn, cancelBtn;

	public SplashScreenClassTest() {
		super("app.mtproject", SplashScreen.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		solo = new Solo(getInstrumentation(), getActivity());
		splashScreenActivity = getActivity();
		instructions = (TextView) solo.getView(R.id.splashInstructions);
		acceptBtn = (Button) solo.getView(R.id.acceptBtn);
		cancelBtn = (Button) solo.getView(R.id.cancelBtn);
	}

	@Override
	protected void tearDown() throws Exception {
		try {
			solo.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		getActivity().finish();
		super.tearDown();
	}
	
	public void testCheckViews() throws Exception {
		assertNotNull(instructions);
		assertNotNull(acceptBtn);
		assertNotNull(cancelBtn);
	}
}
