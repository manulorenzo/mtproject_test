package app.mtproject.test;

import android.content.Intent;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.test.ActivityInstrumentationTestCase2;
import android.view.Menu;
import android.widget.ToggleButton;
import app.mtproject.MtprojectActivity;
import org.easymock.EasyMock;
import org.junit.Test;
import org.junit.Test.None;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.modules.junit4.PowerMockRunner;
import android.view.Menu;
import android.view.MenuItem;
import app.mtproject.*;
import app.mtproject.R;

import com.jayway.android.robotium.solo.Solo;

@RunWith(PowerMockRunner.class)
public class MtProjectActivityTest extends ActivityInstrumentationTestCase2<MtprojectActivity> {
	Menu menu = PowerMock.createMock(Menu.class);
	MenuItem menuItem = PowerMock.createMock(MenuItem.class);
	ToggleButton callsButton, smsButton, locationButton;
	OnSharedPreferenceChangeListener listener;
	MtprojectActivity mActivity;
	private Solo solo;

	public MtProjectActivityTest() {
		super("app.mtproject", MtprojectActivity.class);
	}

	public void testPreConditions() {
		assertNotNull(callsButton);
		assertNotNull(smsButton);
		assertNotNull(locationButton);
	}

	protected void setUp() throws Exception {
		super.setUp();
		setActivityInitialTouchMode(false);
		Intent i = new Intent();
		i.putExtra("username", "manu");
		setActivityIntent(i);
		mActivity = (MtprojectActivity) getActivity();

		solo = new Solo(getInstrumentation(), getActivity());

		callsButton = (ToggleButton) mActivity
				.findViewById(app.mtproject.R.id.callsLoggingBtn);
		smsButton = (ToggleButton) mActivity
				.findViewById(app.mtproject.R.id.smsLoggingBtn);
		locationButton = (ToggleButton) mActivity
				.findViewById(app.mtproject.R.id.locationLogginBtn);
	}

	protected void tearDown() throws Exception {
		try {
			solo.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		getActivity().finish();
		super.tearDown();
	}

	public void testPreferences() throws Exception {
		Menu menu = PowerMock.createMock(Menu.class);
		MenuItem menuItem = PowerMock.createMock(MenuItem.class);

		EasyMock.expect(menu.add(0, 1, Menu.NONE, R.string.preferences)).andReturn(menuItem);

		PowerMock.replayAll();
		assertTrue(mActivity.onCreateOptionsMenu(menu));
		PowerMock.verifyAll();
	}
}
