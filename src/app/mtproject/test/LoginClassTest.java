package app.mtproject.test;

import org.junit.Assert;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import app.mtproject.Login;
import app.mtproject.R;
import com.jayway.android.robotium.solo.Solo;

public class LoginClassTest extends ActivityInstrumentationTestCase2<Login> {
	String username = "manu";
	Login loginActivity;
	EditText un, pw;
	Button ok;
	TextView error;
	private Solo solo;

	public LoginClassTest() {
		super("app.mtproject", Login.class);
	}

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		solo = new Solo(getInstrumentation(), getActivity());
		loginActivity = getActivity();
		un = (EditText) solo.getView(R.id.et_un);
		pw = (EditText) solo.getView(R.id.et_pw);
		ok = (Button) solo.getView(R.id.btn_login);
		error = (TextView) solo.getView(R.id.tv_error);
	}

	@Override
	public void tearDown() throws Exception {
		try {
			solo.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
		getActivity().finish();
		super.tearDown();
	}

	public void testCheckLoginInput() throws Exception {
		solo.enterText(un, username);
		solo.enterText(pw, username);
		solo.clickOnButton("Login");
	}

	public void testCheckViews() throws Exception {
		assertNotNull(un);
		assertNotNull(pw);
		assertNotNull(ok);
		assertNotNull(error);
	}
	
	public void testCheckHttpConnection() throws Exception {
		
	}
}
